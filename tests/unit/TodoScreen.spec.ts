import { waitFor } from "@testing-library/vue";
import { mount } from "@vue/test-utils";
import TodoScreen from "@/components/TodoScreen.vue";
import { apis } from "@/services/api-service";

apis.getTodoLisDoRequest = jest
  .fn()
  .mockImplementation(() => {
    return {
      text: "mock data 1"
    }
  });

describe("TodoScreen unit test:", () => {
  test("check add todo page", async () => {
    const wrapper = mount(TodoScreen, {
      propsData: {
        items: {
          task: "run",
          done: false,
        },
      },
    });

    const button = await waitFor(() => wrapper.find("button"))
    const form = await waitFor(() => wrapper.find("form"))
    const h2 = await waitFor(() => wrapper.find("h2"))

    expect(button.text()).toBe("Add Todo List");
    expect(form.exists()).toBe(true);
    expect(h2.text()).toBe("Todo App");
  });

  test('check title', () => {
    const wrapper = mount(TodoScreen)
    const title = wrapper.find('.app .h2')
    expect(title.text()).toBe("Todo App")
  })

  test('check input area', () => {
    const wrapper = mount(TodoScreen)
    const inputArea = wrapper.find('.app .input')
    expect(inputArea.exists()).toBe(true)
  })

  test('check and rendering add button', () => {
    const wrapper = mount(TodoScreen)
    const addButton = wrapper.find('.app .button')
    expect(addButton.exists()).toBe(true)
  })
});





