/**
 * @jest-environment node
 */

const { Pact } = require("@pact-foundation/pact");
const { like, eachLike } = require("@pact-foundation/pact").Matchers;
import TodoScreen from "../../src/components/TodoScreen.vue";
import { apis } from "../../src/services/api-service";
const path = require('path');

const provider = new Pact({
    consumer: 'consumer',
    provider: 'provider',
    port: 8080,
    log: path.resolve(`${process.cwd()}/contract`, 'logs', 'pact.log'),
    logLevel: 'warn',
    dir: path.resolve(`${process.cwd()}/contract`, 'pacts'),
    spec: 2
});
describe('API Pact test', () => {

    beforeAll(() => provider.setup());
    afterEach(() => provider.verify());
    afterAll(() => provider.finalize());

    describe('getting all todos', () => {
        test('todos exists', async () => {

            // set up Pact interactions
            await provider.addInteraction({
                state: 'todo exists',
                uponReceiving: 'get all todos',
                withRequest: {
                    method: 'GET',
                    path: '/api',

                },
                willRespondWith: {
                    status: 200,
                    headers: {
                        'Content-Type': 'application/json; charset=utf-8'
                    },
                    body: eachLike({
                        text: like("Movie 1"),
                    }),
                },
            });

            // make request to Pact mock server
            const response = await TodoScreen.methods.getItems();
            expect(response[0].text).toBe("Movie 1");
        });
    })


    // Add TODO part is not working right now

    // describe('add todo part', () => {
    //     test('add Movie 1 a todo', async () => {
    //         let todoTobeAdded = { text: "Movie 1" }
    //         //let expectedResponse = new Return('todo created and added.', todoTobeAdded)
    //         // set up Pact interactions
    //         await provider.addInteraction({
    //             state: ' todo object',
    //             uponReceiving: 'add a todo',
    //             withRequest: {
    //                 method: 'POST',
    //                 path: '/api/add-todo',
    //                 body: todoTobeAdded

    //             },
    //             willRespondWith: {
    //                 status: 200,
    //                 headers: {
    //                     'Content-Type': 'application/json; charset=utf-8'
    //                 },
    //                 body: eachLike({
    //                     text: like("Movie 1"),
    //                 }),
    //             },
    //         });


    //         // make request to Pact mock server
    //         const product = await apis.addTodoDoRequest();

    //         expect(product).toEqual(todoTobeAdded);
    //     });
    // })

})

