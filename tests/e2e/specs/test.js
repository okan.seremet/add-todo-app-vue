
describe("Add ToDo List Test", () => {
  it("Go app url", () => {
    cy.visit("/");
    cy.contains("h2", "Todo App");
    cy.get("form");
  });
  it("add todo to list", () => {
    cy.visit("/");
    cy.get("input.input").type("any todo");
    cy.get("button.button").trigger("click");
    cy.get("ul");
  });
});
