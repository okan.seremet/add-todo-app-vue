import Vue from 'vue'
import VueRouter, { RouteConfig } from 'vue-router'
import TodoScreenPage from '../views/TodoScreenPage.vue'

Vue.use(VueRouter)

const routes: Array<RouteConfig> = [
  {
    path: '/',
    name: 'TodoScreenPage',
    component: TodoScreenPage
  },
  {
    path: '/todo',
    name: 'TodoScreenPage',
    component: () => import(/* webpackChunkName: "about" */ '../views/TodoScreenPage.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
