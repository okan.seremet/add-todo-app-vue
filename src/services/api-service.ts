import axios from "axios";

const domain =  'http://localhost:8080';
const baseGetTodoListUrl = `${domain}/api`;
const baseAddTodoUrl = `${domain}/api/add-todo`

const getTodoLisDoRequest = async () => {
    const response = axios.get(baseGetTodoListUrl)
    return await response;
}
 
const addTodoDoRequest = async (text:string) => {
    const response =  axios.post(baseAddTodoUrl,{text:text})
    return await response;
}

export const apis = {getTodoLisDoRequest,addTodoDoRequest}